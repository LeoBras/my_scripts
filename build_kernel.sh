#!/bin/bash

cd ~/local_kernel
CMD="make -j$(nproc)"

make olddefconfig
time ${CMD}

KVER=v$(make kernelversion)

time sudo ${CMD} modules_install

sudo cp arch/$(uname -m)/boot/bzImage /boot/vmlinuz-${KVER}
sudo mkinitcpio  -k /boot/vmlinuz-${KVER}  -g /boot/initramfs-${KVER}.img
sudo cp System.map /boot/System.map-${KVER}
sudo grub-mkconfig -o /boot/grub/grub.cfg

